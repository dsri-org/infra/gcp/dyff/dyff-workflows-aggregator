# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.20.0"
  constraints = "~> 5.20.0"
  hashes = [
    "h1:L4INA+OBH9Go2roPtN9UIE/rBLi9aJ0hp0PGQmrTfc4=",
    "zh:124d7577793fe37fcc5228039eb478061fd0863a15eb3e1570db44005f69382c",
    "zh:3c500b7cfbb51b216f738797db2ed7e91ad646c1c782561c4333fcfe828ef80a",
    "zh:463a439976fd0e97205c750e83915bac7118605d40b4871e7c41df712d1e4c83",
    "zh:5802953a3dd08e6c27f5f2d8f6cf4ee7b20db46494054abf4ddb2d5c355450c8",
    "zh:5e54e697020d2d48db792b8a6b9ac8f0b23d955c00d2a2a5a9d08731da62abea",
    "zh:778019fdf28ba42c605ab82353a6ce6117d4091adf3227c7f63ee555fe2e5d48",
    "zh:b8958ec729dd034d8c4d8a4c8184da958cfd2b52df01b0aede120158cc6b65ca",
    "zh:c8e8c87653cdda0855d3b0f1c8924c47280a6ce6bb313fd76169280f2cc7320b",
    "zh:ec251ee722efaa4c8501d51eceb7075a12bded8ade4d479074a4dfc777976269",
    "zh:ec69b42faf218e015556ab1d55ea47b4bcb5b76754ee8209a950574250de7db5",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.12.1"
  constraints = "~> 2.12.1"
  hashes = [
    "h1:S0+5VN/viVA4YYpm9q45bZ903EqP3bwjv5abps+a3lE=",
    "zh:0349149992646530c33314cb973eba68757606a037017ba47e56db695d4b3afe",
    "zh:3138ffe23c481b01419a4a21adf83538efe6e698b421c4a8f7d142b198518709",
    "zh:44658e3070405b88fbd76161ecddde62f478dc31aaebee3b93c2f2783a6d45f9",
    "zh:5600a3407dfb8b77da7561490157afa8ad505c864a5dd35ed8d678e9ad8378ca",
    "zh:6445e359c813ecbb7c2edf722ed0d1f33dfb171b6a7b470f40cf1e24045b7441",
    "zh:7973054604c7f5a51600f6e63fa0327d05b29fac2bffd222c21660cbdd2939f9",
    "zh:7c59e2d4602ab5d9de0ba8e442ec1fc425c8f143581018d1e7f645298a124f01",
    "zh:8c0fb411dd5de664ac5e801d70507781790c4fc196518a56966d66d0963c240c",
    "zh:a6a988c91bbf1828a8fc55001f10c7d06c5c53dc718ee7cd6814bdfa2e6652e0",
    "zh:b7935d7dacd7e5a91ff9d17cfb04ce88c9100e563fd88487d14519e8d8d8b2e1",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.27.0"
  constraints = "~> 2.27.0"
  hashes = [
    "h1:Jtbdvbq8kIXUENtH3tVwgcjHqbuYp1pGfg4gFocY+e4=",
    "zh:1146f53fb39fd4bcea5574303c4871001a97d7891f65a60a4ecbc64da2a90d75",
    "zh:1f7e3dc0dbb854f56a0f5ba3c50588272984ae9775da027c3c7f32cb6d8245b0",
    "zh:2166f7fdade75266658603280bc822edab848e52a674340485847dde1c5d9324",
    "zh:21a97530857330d2013aa66fb7afebb44fe4a5543418d0a3ca93750acd11fea5",
    "zh:2d4b9fea7e99750647e1cd8df9a67cba45905825867dd19ab01411dad6b8c6fd",
    "zh:de30e92e638b95e56dbb2232cb9a6f6a69346ecb3644965e9be715eaf29f22ff",
    "zh:f4ae951c9add4349a498f44c3f5768cbaf7a966392a0e7632de288889e7cd5d9",
    "zh:f54ecb1917dfa198933d72632ea6f0aa4da3ead070d6b9765ec1d3b7da60e827",
    "zh:fba8a2f192eb5fe248708b9037db046e0d9176e7c54c6edc6f6aa55d50474082",
    "zh:fe525956f3e54f0bbd2891a6abad1f807b4763b8dc734d810e223876741fefa3",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:tW+G7lgqbHUtraKHPWuotYHlME1vcAf50YvOeHQlGHg=",
    "zh:0002dd4c79453da5bf1bb9c52172a25d042a571f6df131b7c9ced3d1f8f3eb44",
    "zh:49b0f8c2bd5632799aa6113e0e46acaa7d008f927665a41a1f8e8559fe6d8165",
    "zh:56df70fca236caa06d0e636c41ab71dd1ced05375f4ddcb905b0ed2105737048",
    "zh:58e4de40540c86b9e2e2595dac1318ba057718961a467fa9727866f747693eb2",
    "zh:5992f11c738812ccd7476d4c607cb8b76dea5aa612be491150c89957ec395ddd",
    "zh:7ff4f0b7707b51737f684e96d85a47f0dd8be0f72a3c27b0798755d3faad15e2",
    "zh:8e4b0972e216c9773ab525accfa36eb27c44c751b06b125ecc53f4226c91cea8",
    "zh:d8956cc5abcd5d1173b6cc25d5d8ed2c5cc456edab2fddb774a17d45e84820cb",
    "zh:df7f9eb93a832e66bc20cc41c57d38954f87671ec60be09fa866273adb8d9353",
    "zh:eb583d8f03b11f0b6c535375d8ed0d29e5f7f537b5c78943856d2e8ce76482d9",
  ]
}
