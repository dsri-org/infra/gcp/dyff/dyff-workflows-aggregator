# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "random_string" "application_id" {
  length  = 32
  special = false
}

resource "kubernetes_namespace" "workflows_aggregator" {
  metadata {
    name = "workflows-aggregator"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/workflows-aggregator/workflows-aggregator
resource "helm_release" "workflows_aggregator" {
  name       = "workflows-aggregator"
  namespace  = kubernetes_namespace.workflows_aggregator.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "workflows-aggregator"
  version    = "0.5.0"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    replicaCount = 1

    extraEnvVarsConfigMap = {
      DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS = local.kafka.bootstrap_servers
      DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS  = local.kafka.topics_map["dyff.workflows.events"].name
      DYFF_KAFKA__TOPICS__WORKFLOWS_STATE   = local.kafka.topics_map["dyff.workflows.state"].name
      DYFF_KAFKA__STREAMS__APPLICATION_ID   = random_string.application_id.result
      DYFF_KAFKA__STREAMS__STATE_DIR        = "/kafka/state"

      DYFF_STORAGE__BACKEND               = "dyff.storage.backend.s3.storage.S3StorageBackend"
      DYFF_STORAGE__S3__ENDPOINT          = local.storage.hostname
      DYFF_STORAGE__S3__INTERNAL_ENDPOINT = local.storage.hostname
      DYFF_STORAGE__S3__ACCESS_KEY        = local.storage.access_id

      DYFF_RESOURCES__DATASETS__STORAGE__URL     = local.storage.buckets["datasets"].s3_url
      DYFF_RESOURCES__MEASUREMENTS__STORAGE__URL = local.storage.buckets["measurements"].s3_url
      DYFF_RESOURCES__MODELS__STORAGE__URL       = local.storage.buckets["models"].s3_url
      DYFF_RESOURCES__MODULES__STORAGE__URL      = local.storage.buckets["modules"].s3_url
      DYFF_RESOURCES__OUTPUTS__STORAGE__URL      = local.storage.buckets["outputs"].s3_url
      DYFF_RESOURCES__REPORTS__STORAGE__URL      = local.storage.buckets["reports"].s3_url
      DYFF_RESOURCES__SAFETYCASES__STORAGE__URL  = local.storage.buckets["safetycases"].s3_url
    }

    extraEnvVarsSecret = {}

    volumes = [{
      name     = "tmpfs",
      emptyDir = {}
      }, {
      name = "kafka-state",
      ephemeral = {
        volumeClaimTemplate = {
          spec = {
            storageClassName = "regional-standard-rwo"
            accessModes      = ["ReadWriteOnce"],
            resources = {
              requests = {
                storage = "10Gi"
              }
            }
          }
        }
      }
    }]

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }
  })]

  set_sensitive {
    name  = "extraEnvVarsSecret.DYFF_STORAGE__S3__SECRET_KEY"
    value = local.storage.secret
  }
}
